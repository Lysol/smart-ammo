#!/bin/bash
set -eu -o pipefail

file_name=smart-ammo.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

test -d docs || mkdir docs

for f in CHANGELOG.md LICENSE README.md; do
    mv $f docs/
done

zip -r ${file_name} docs l10n scripts smart-ammo.omwscripts version.txt
sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt

for f in CHANGELOG.md LICENSE README.md; do
    mv docs/${f} .
done
