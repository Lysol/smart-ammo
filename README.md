# Smart Ammo for OpenMW-Lua

This is a port of [Abot's Smart Ammo mod](https://www.nexusmods.com/morrowind/mods/47383) for OpenMW-Lua.

#### Credits

##### Original concept, MWSE-Lua author

Abot

##### OpenMW-Lua author

johnnyhostile

##### Localizations

DE: Atahualpa

EN: johnnyhostile

ES: drumvee

NL: Hitobaka

PL: stahs, emcek

SV: Lysol

ZH, ZH-CN: noctune

#### Features

* Auto-equips the correct ammo when a marksman-type weapon is equipped...
* ... and when your current ammo runs out, it's auto-restocked with another of the correct type (if you have any).
* Settings menu allows toggling messages on or off (on by default).

#### Installation

**OpenMW 0.48 or newer is required!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/smart-ammo/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Gameplay\SmartAmmo

        # Linux
        /home/username/games/OpenMWMods/Gameplay/SmartAmmo

        # macOS
        /Users/username/games/OpenMWMods/Gameplay/SmartAmmo

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Gameplay\SmartAmmo"`)
1. Add `content=smart-ammo.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher
1. Enjoy!

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/smart-ammo/-/issues)
* Email `smart-ammo@modding-openmw.com`
* Contact the author on Discord: `johnnyhostile#6749`
* Contact the author on Libera.chat IRC: `johnnyhostile`

#### Planned Features

[Request a feature!](https://gitlab.com/modding-openmw/smart-ammo/-/issues/new)
