## Smart Ammo for OpenMW-Lua Changelog

#### 1.2

* Added SV localization (thanks Lysol!)

<!-- [Download Link](https://gitlab.com/modding-openmw/smart-ammo/-/packages/7662875) | [Nexus](https://www.nexusmods.com/morrowind/mods/51274) -->

#### 1.1

* Fixed a Lua crash caused by holding a lockpick or probe ([issue !1](https://gitlab.com/modding-openmw/smart-ammo/-/issues/1))
* Added DE, PL, and ES localizations
* Other minor changes to the website and documentation

[Download Link](https://gitlab.com/modding-openmw/smart-ammo/-/packages/7662875) | [Nexus](https://www.nexusmods.com/morrowind/mods/51274)

#### 1.0

Initial mod release.

Features:

* Auto-equips the correct ammo when a marksman-type weapon is equipped...
* ... and when your current ammo runs out, it's auto-restocked with another of the correct type (if you have any).
* Settings menu allows toggling messages on or off (on by default).

[Download Link](https://gitlab.com/modding-openmw/smart-ammo/-/packages/6910444) | [Nexus](https://www.nexusmods.com/morrowind/mods/51274)
